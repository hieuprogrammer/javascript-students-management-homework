var student = {
    id: ``,
    name: ``,
    income: ``,
    math: ``,
    literature: ``,

    calculateAverageScore: function() {
        return (Number(this.math) + Number(this.literature)) / 2;
    },

    calculateGrade: function() {
        var average = this.calculateAverageScore();
        var grade = ``;

        if (average >= 5) {
            grade = `PASSED`;
        } else {
            grade = `FAILED`;
        }

        return grade;
    }
}

document.querySelector(`button.btn`).onclick = function() {
    student.id = document.querySelector(`#txtId`).value;
    student.name = document.querySelector(`#txtName`).value;
    student.income = document.querySelector(`#txtIncome`).value;
    student.math = document.querySelector(`#txtMath`).value;
    student.literature = document.querySelector(`#txtLiterature`).value;

    document.querySelector(`#spanId`).innerHTML = student.id;
    document.querySelector(`#spanName`).innerHTML = student.name;
    document.querySelector(`#spanIncome`).innerHTML = student.income;
    document.querySelector(`#spanAverageScore`).innerHTML = student.calculateAverageScore();
    document.querySelector(`#spanGrade`).innerHTML = student.calculateGrade();
}